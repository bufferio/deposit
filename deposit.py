# coding: utf-8
import datetime
import hashlib
import urllib2
import uuid
import os

import flask
from flaskext import sqlalchemy
from sqlalchemy import func, or_, Table


DEBUG = True
SECRET_KEY = "foobarbaz"
SQLALCHEMY_DATABASE_URI = "sqlite:///deposit.db"

DEPOSIT_SECRET = "foobarbaz"
DEPOSIT_TITLE = "Deposit"
DEPOSIT_ARCHIVE_DIR = "static/archive"
DEPOSIT_ARCHIVE_URL = "static/archive"

TEMPLATE_GLOBALS = {"archive_url": DEPOSIT_ARCHIVE_URL,
                    "title": DEPOSIT_TITLE}

app = flask.Flask(__name__)
app.config.from_object(__name__)
db = sqlalchemy.SQLAlchemy(app)

### Models
class Item(db.Model):
    __tablename__ = "table"

    id = db.Column(db.Integer, primary_key=True)
    pub_date = db.Column(db.DateTime)
    filename = db.Column(db.String(40))

    def __init__(self, filename):
        self.filename = filename
        self.pub_date = datetime.datetime.utcnow()

### Template
def datetimeformat(value, format="%H:%M / %d-m-%m-%Y"):
    return value.strftime(format)

app.jinja_env.filters['datetimeformat'] = datetimeformat

### Helpers
def fetch_file(url):
    """Fetch resource from URL, rename and paste into folder, return
       the new filename."""
  
    # naively determine extension
    extension = url.split(".")[-1]

    # random uuid
    name = str(uuid.uuid1())
 
    data_source = urllib2.urlopen(url)
    data_dest = open(os.path.join(DEPOSIT_ARCHIVE_DIR, "%s.%s" % (name, extension)), "w")

    data_dest.write(data_source.read())

    data_source.close()
    data_dest.close()

    return "%s.%s" % (name, extension)

def get_items():
    query = Item.query.order_by(Item.pub_date.desc())
    return query

### Routes
@app.route("/")
def index():
    return show_items(1)

@app.route("/<int:page>")
def show_items(page, author=None, tags=None):
    page = get_items().paginate(page, 10, page != 1)
    return flask.render_template("show_items.html", page=page,
                                 **TEMPLATE_GLOBALS)

@app.route("/add", methods=["POST"])
def add_item():
    if flask.request.form["secret"] != DEPOSIT_SECRET:
        flask.abort(403)

    # process filename, create instance of item and db.session.commit
    filename = fetch_file(flask.request.form["url"])

    item = Item(filename)
    db.session.add(item)
    db.session.commit()

    response = flask.make_response()
    response.headers["Access-Control-Allow-Origin"] = "*"
    return response

if __name__ == '__main__':
    import sys
    args = sys.argv[1:]
    if args:
        if args[0] == "add_item":
            # process filename, create instance of item and db.session.add, commit
            filename = fetch_file(args[1])

            item = Item(filename)
            db.session.add(item)
            db.session.commit()
        elif args[0] == "init_db":
            db.create_all()
        else:
            app.run(args[0], int(args[1]))
    else:
        app.run()
